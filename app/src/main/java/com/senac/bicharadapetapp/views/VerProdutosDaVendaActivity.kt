package com.senac.bicharadapetapp.views

import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.senac.bicharadapetapp.R
import com.senac.bicharadapetapp.model.ItemCarrinho
import com.senac.bicharadapetapp.model.ProdutosVenda
import kotlinx.android.synthetic.main.activity_ver_produtos_da_venda.*
import kotlinx.android.synthetic.main.activity_visualizar_carrinho.*
import kotlinx.android.synthetic.main.card_item_produtos_venda.view.*
import java.text.NumberFormat

class VerProdutosDaVendaActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ver_produtos_da_venda)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED;
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if(ProdutosVenda.produtosVenda != null && ProdutosVenda.produtosVenda.size >= 1){
            montaTelaProdutosVenda(ProdutosVenda.produtosVenda)
        } else {
            Snackbar
                .make(containerCarrinho, "Um erro ocorreu, a lista de produtos está vazia.", Snackbar.LENGTH_LONG)
                .show()
        }
    }

    fun montaTelaProdutosVenda(listaProdutos: List<ItemCarrinho>){
        if(listaProdutos != null){
            val formater = NumberFormat.getCurrencyInstance()
            containerVerProdutosVenda.removeAllViews()
            for(produtoDaVenda in listaProdutos){
                val cardView = layoutInflater
                    .inflate(R.layout.card_item_produtos_venda, containerVerProdutosVenda, false)
                cardView.txtNomeProdutosVenda.text = produtoDaVenda.produto.nomeProduto
                cardView.txtPrecoUnitarioProdutosVenda.text = formater.format(produtoDaVenda.produto.precProduto)
                cardView.txtQuantidadeProdutosDaVenda.text = produtoDaVenda.quantidade.toString()
                containerVerProdutosVenda.addView(cardView)
            }

        }

    }
}
