package com.senac.bicharadapetapp.views

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.senac.bicharadapetapp.R
import com.senac.bicharadapetapp.model.ItemCarrinho
import com.senac.bicharadapetapp.model.Produto
import com.senac.bicharadapetapp.model.ProdutosVenda
import com.senac.bicharadapetapp.model.Venda
import kotlinx.android.synthetic.main.activity_listagem_vendas.*
import kotlinx.android.synthetic.main.card_item_venda.view.*
import java.text.NumberFormat

class ListagemVendasActivity : AppCompatActivity() {

    var database: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listagem_vendas)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED;
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if(getCurrentUser() != null){
            configurarFirebaseDados()
        }

    }

    fun obterVendas(dataSnapshot: DataSnapshot){
        if(getCurrentUser() !=  null){
            val listaVendas = arrayListOf<Venda>()
            dataSnapshot.child("venda").children.forEach{
                val listaProdutosFinal = arrayListOf<ItemCarrinho>()
                val nohAtual = it
                val mapa = nohAtual.getValue() as HashMap<String, Any>
                val id = mapa.get("id") as String
                val dataVenda = mapa.get("dataVenda") as String
                val totalVenda =  retornaValorParaDouble(mapa.get("totalVenda"))
                val qtdProdutos = retornaValorParaInteger(mapa.get("qtdTotalItensCarrinho"))
                    nohAtual.child("produtos").children.forEach{
                    val nohProdutoAtual = it
                    val mapaProduto = nohProdutoAtual.getValue() as HashMap<String, Any>
                    val quantidade = retornaValorParaInteger(mapaProduto.get("quantidade"))
                    val nohItem = nohProdutoAtual.child("produto")
                        val mapItens = nohItem.getValue() as HashMap<String, Any>
                        val nomeProduto = mapItens.get("nomeProduto") as String
                        val descProduto = mapItens.get("descProduto") as String
                        val precProduto = retornaValorParaDouble(mapItens.get("precProduto"))
                        val idCategoria = retornaValorParaInteger(mapItens.get("idCategoria"))
                        val qtdMinEstoque = retornaValorParaInteger(mapItens.get("qtdMinEstoque"))
                        val ativoProduto = mapItens.get("ativoProduto") as Boolean
                        val idProduto = retornaValorParaInteger(mapItens.get("idProduto"))
                        val descontoPromocao = retornaValorParaDouble(mapItens.get("descontoPromocao"))
                        val produtoFinal = Produto(nomeProduto, descProduto, precProduto, idCategoria, qtdMinEstoque, ativoProduto, idProduto, descontoPromocao)
                    val itemCarrinhoFinal = ItemCarrinho(quantidade,produtoFinal)
                    listaProdutosFinal.add(itemCarrinhoFinal)
                }
                val venda = Venda(id = id, dataVenda = dataVenda, totalVenda = totalVenda, produtos = listaProdutosFinal, qtdTotalItensCarrinho = qtdProdutos)
                listaVendas.add(venda)
            }
            atualizarTela(listaVendas)
        }
    }

    fun atualizarTela(listaVendas: List<Venda>){
        if(listaVendas != null){
            var index = 0
            val formater = NumberFormat.getCurrencyInstance()
            for(venda in listaVendas){
                val cardView = layoutInflater
                    .inflate(R.layout.card_item_venda, containerVendas, false)
                cardView.txtDataDaVendaCard.text = venda.dataVenda
                cardView.txtValorVendaCard.text = formater.format(venda.totalVenda)
                cardView.txtQuantidadeItensCard.text = venda.qtdTotalItensCarrinho.toString()
                index++
                cardView.btnVerProdutosDaVenda.setOnClickListener{
                    ProdutosVenda.produtosVenda.clear()
                    ProdutosVenda.produtosVenda.addAll(venda.produtos)
                    val intentProdutosVenda = Intent(this@ListagemVendasActivity, VerProdutosDaVendaActivity::class.java)
                    startActivity(intentProdutosVenda)
                }
                containerVendas.addView(cardView)
            }
        }
    }

    fun retornaValorParaDouble(valorLong: Any?): Double {
        var valorRetorno = 0.0
        if (valorLong is Long){
            valorRetorno = valorLong.toDouble()
            return valorRetorno
        } else if (valorLong is Double){
            return valorLong
        }
        return valorRetorno
    }

    fun retornaValorParaInteger(valorLong: Any?): Int {
        var valorRetorno = 0
        if (valorLong is Long){
            valorRetorno = valorLong.toInt()
            return valorRetorno
        } else if (valorLong is Int){
            return valorLong
        }
        return valorRetorno
    }

    fun configurarFirebaseDados() {
        val usuario = getCurrentUser()
        if(getCurrentUser() != null){
            if (usuario != null) {
                database = FirebaseDatabase.getInstance().reference.child(usuario.uid)
            }
            val callback = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    obterVendas(dataSnapshot)
                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            }
            database?.addValueEventListener(callback)
        }
    }

    fun getCurrentUser(): FirebaseUser?{
        val auth = FirebaseAuth.getInstance()
        return auth.currentUser
    }
}
