package com.senac.bicharadapetapp.views

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.senac.bicharadapetapp.R
import com.senac.bicharadapetapp.model.Produto
import com.senac.bicharadapetapp.services.ProdutoService
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detalhe_produto.*
import kotlinx.android.synthetic.main.activity_tela_menu_inicial.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.NumberFormat

class DetalheProdutoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhe_produto)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED;
        val formater = NumberFormat.getCurrencyInstance()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Carregando detalhes do produto.")
        progressDialog.setCancelable(false)
        progressDialog.show()
        btnAdicionarCarrinhoPorDetakhe.setOnClickListener{
            val intentCarrinho = Intent(this@DetalheProdutoActivity, AdicionaCarrinhoActivity::class.java)
            intentCarrinho.putExtra("idProduto", intent.getIntExtra("idProduto",0))
            startActivityForResult(intentCarrinho, 0)
        }
        val retrofit = Retrofit.Builder()
            .baseUrl("https://oficinacordova.azurewebsites.net")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(ProdutoService::class.java)
        val call = service.obterPorId(intent.getIntExtra("idProduto",0))
        val callback = object : Callback<Produto> {

            override fun onResponse(call: Call<Produto>, response: Response<Produto>) {
                if(response.isSuccessful){
                    var produto = response.body()
                    txtNomeDetalhe.text = produto?.nomeProduto
                    txtPrecoDetalhe.text = formater.format(produto?.precProduto)
                    txtDescontoDetalhe.text = formater.format(produto?.descontoPromocao)
                    Picasso.get().load("https://oficinacordova.azurewebsites.net/android/rest/produto/image/"
                            + produto?.idProduto)
                        .into(imageView)
                    txtIdProdDetalhe.text = produto?.descProduto
                    Handler().postDelayed({progressDialog.dismiss()}, 500)
                } else{
                    Snackbar
                        .make(containerMenu, "Não foi possível carregar detalhes do produto", Snackbar.LENGTH_LONG)
                        .show()
                    Log.e("ERRO", response.errorBody().toString())
                    Handler().postDelayed({progressDialog.dismiss()}, 500)
                }
            }
            override fun onFailure(call: Call<Produto>, t: Throwable) {
                Snackbar
                    .make(containerMenu, "Não foi possível se conectar a internet", Snackbar.LENGTH_LONG)
                    .show()
                Log.e("ERRO", "Falha ao chamar o serviço", t)
                Handler().postDelayed({progressDialog.dismiss()}, 500)
            }

        }
        call.enqueue(callback)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 0){
            if(resultCode == Activity.RESULT_OK){
                Toast.makeText(this, "O produto " + data?.getStringExtra("nomeProduto") + " foi adicionado com sucesso ao carrinho.", Toast.LENGTH_LONG).show()
            }
        }
    }

}
