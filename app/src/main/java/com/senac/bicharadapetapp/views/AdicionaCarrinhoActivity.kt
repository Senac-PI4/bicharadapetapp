package com.senac.bicharadapetapp.views

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import com.senac.bicharadapetapp.R
import com.senac.bicharadapetapp.model.Carrinho
import com.senac.bicharadapetapp.model.ItemCarrinho
import com.senac.bicharadapetapp.model.Produto
import com.senac.bicharadapetapp.services.ProdutoService
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_adiciona_carrinho.*
import kotlinx.android.synthetic.main.activity_tela_menu_inicial.*
import kotlinx.android.synthetic.main.activity_visualizar_carrinho.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.NumberFormat
import kotlin.math.pow

class AdicionaCarrinhoActivity : AppCompatActivity() {
    var produtoItemCarrinho = Produto("","", Double.NaN, 0, 0, true, 1, Double.NaN)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adiciona_carrinho)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED;
        val formater = NumberFormat.getCurrencyInstance()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Carregando detalhes do produto.")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val retrofit = Retrofit.Builder()
            .baseUrl("https://oficinacordova.azurewebsites.net")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(ProdutoService::class.java)
        val call = service.obterPorId(intent.getIntExtra("idProduto",0))
        val callback = object : Callback<Produto> {

            override fun onResponse(call: Call<Produto>, response: Response<Produto>) {
                if(response.isSuccessful){
                    var produto = response.body()
                    if (produto != null) {
                        produtoItemCarrinho = produto
                    }
                    txtNomeCarrinho.text = produto?.nomeProduto
                    txtPrecoAddCarrinho.text = formater.format(produto?.precProduto)
                    txtDescAddCarrinho.text = formater.format(produto?.descontoPromocao)
                    Picasso.get().load("https://oficinacordova.azurewebsites.net/android/rest/produto/image/"
                            + produto?.idProduto)
                        .into(imgAddCarrinho)
                    Handler().postDelayed({progressDialog.dismiss()}, 500)
                } else{
                    Snackbar
                        .make(containerMenu, "Não foi possível carregar detalhes do produto", Snackbar.LENGTH_LONG)
                        .show()
                    Log.e("ERRO", response.errorBody().toString())
                    Handler().postDelayed({progressDialog.dismiss()}, 500)
                }
            }
            override fun onFailure(call: Call<Produto>, t: Throwable) {
                Snackbar
                    .make(containerMenu, "Não foi possível se conectar a internet", Snackbar.LENGTH_LONG)
                    .show()
                Log.e("ERRO", "Falha ao chamar o serviço", t)
                Handler().postDelayed({progressDialog.dismiss()}, 500)
            }

        }
        call.enqueue(callback)

        btnAddAoCarrinho.setOnClickListener{
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Adicionar ao carrinho")
            builder.setMessage("Quer mesmo adicionar o item ao carrinho?")
            builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                adicionaItemNoCarrinho()
            }
            builder.setNegativeButton(android.R.string.no) { dialog, which ->

            }
            builder.show()
        }
    }

    fun adicionaItemNoCarrinho(){
        if(etQuantidadeCarrinho.text.toString() == "" || etQuantidadeCarrinho.text == null){
            Snackbar
                .make(containerAdicionaItemNoCarrinho, "É preciso informar uma quantidade válida.", Snackbar.LENGTH_LONG)
                .show()
        } else if(etQuantidadeCarrinho.text.toString().toInt() <= 0){
            Snackbar
                .make(containerAdicionaItemNoCarrinho, "É preciso informar uma quantidade válida.", Snackbar.LENGTH_LONG)
                .show()
        }
        else {
            var itemCarrinho = ItemCarrinho(etQuantidadeCarrinho.text.toString().toInt(), produtoItemCarrinho)
            Carrinho.carrinholista.add(itemCarrinho)
            var intentRetorno = Intent()
            intentRetorno.putExtra("nomeProduto", produtoItemCarrinho.nomeProduto)
            setResult(Activity.RESULT_OK, intentRetorno)
            finish()
        }
    }
}
