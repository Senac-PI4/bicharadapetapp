package com.senac.bicharadapetapp.views

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.senac.bicharadapetapp.R
import java.lang.Exception

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED;

        supportActionBar?.hide();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        if(getCurrentUser() != null){
            val background = object: Thread() {
                override  fun run() {
                    try {
                        Thread.sleep(5000)
                        val intent = Intent(baseContext, TelaMenuInicialActivity::class.java)
                        startActivity(intent)
                    } catch (e: Exception) {

                    }
                }
            }
            background.start()
        }
    }
    override fun onResume() {
        super.onResume()
        if(getCurrentUser() == null){
            val email = AuthUI.IdpConfig.EmailBuilder().build()
            val providers = arrayListOf(email)
            val intent = AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setIsSmartLockEnabled(false)
                .setAvailableProviders(providers)
                .build()
            startActivityForResult(intent, 0)
        }
    }

    fun getCurrentUser(): FirebaseUser?{
        val auth = FirebaseAuth.getInstance()
        return auth.currentUser
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "Usuário autenticado", Toast.LENGTH_LONG).show()
                val intent = Intent(baseContext, TelaMenuInicialActivity::class.java)
                startActivity(intent)
            }
            } else {
                finishAffinity()
            }
        }

}
