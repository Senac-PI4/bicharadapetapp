package com.senac.bicharadapetapp.views

import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.senac.bicharadapetapp.R
import kotlinx.android.synthetic.main.activity_sobre_aplicacao.*


class SobreAplicacaoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sobre_aplicacao)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED;
        btnIrSenac.setOnClickListener{
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.sp.senac.br")))
        }

        txtEmailVitor.setOnClickListener{
            val emailIntent = Intent(Intent.ACTION_SENDTO)
            emailIntent.data = Uri.parse("mailto:vitor.silva_santos@hotmail.com")
            startActivity(Intent.createChooser(emailIntent, "Fale com Vitor!"))
        }

        txtEmailEduardo.setOnClickListener{
            val emailIntent = Intent(Intent.ACTION_SENDTO)
            emailIntent.data = Uri.parse("mailto:edustapf@gmail.com")
            startActivity(Intent.createChooser(emailIntent, "Fale com Eduardo!"))
        }

    }
}
