package com.senac.bicharadapetapp.views

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import com.firebase.ui.auth.AuthUI
import com.google.android.gms.auth.api.Auth
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.senac.bicharadapetapp.R
import com.senac.bicharadapetapp.model.Categoria
import com.senac.bicharadapetapp.model.Produto
import com.senac.bicharadapetapp.services.CategoriaService
import com.senac.bicharadapetapp.services.ProdutoService
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_tela_menu_inicial.*
import kotlinx.android.synthetic.main.activity_tela_menu_inicial.drawerLayout
import kotlinx.android.synthetic.main.card_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.NumberFormat

class TelaMenuInicialActivity : AppCompatActivity() {

    var toggle: ActionBarDrawerToggle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tela_menu_inicial)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED;
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val itensSpinner = obterCategoriasProduto()
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, itensSpinner)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerFiltroCategoria.adapter = adapter

        spinnerFiltroCategoria.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                if(position == 0){
                    obterProdutos()
                }
                if(position == 1){
                    obterProdutosPorCategoria(1)
                }
                if(position == 2){
                    obterProdutosPorCategoria(2)
                }
                if(position == 3){
                    obterProdutosPorCategoria(3)
                }
                if(position == 4){
                    obterProdutosPorCategoria(4)
                }
                if(position == 5){
                    obterProdutosPorCategoria(5)
                }
                if(position == 6){
                    obterProdutosPorCategoria(6)
                }
                if(position == 7){
                    obterProdutosPorCategoria(7)
                }
                if(position == 8){
                    obterProdutosPorCategoria(20)
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>){
            }
        }

        btnPesquisaNome.setOnClickListener{
            obterProdutosPorNome(etFiltroNome.text.toString())
        }

        toggle = ActionBarDrawerToggle(this, drawerLayout,
            R.string.openDrawer, R.string.closeDrawer)

        drawerLayout.addDrawerListener(toggle!!)

        toggle!!.syncState()

        navigationVw.setNavigationItemSelectedListener {
            drawerLayout.closeDrawers()

            if(it.getItemId() === R.id.mnSobre ){
                val i = Intent(this@TelaMenuInicialActivity, SobreAplicacaoActivity::class.java)
                startActivity(i)
                true
            }

            if(it.getItemId() === R.id.mnCarrinho){
                val i = Intent(this@TelaMenuInicialActivity, VisualizarCarrinhoActivity::class.java)
                startActivityForResult(i,2)
            }

            if(it.getItemId() === R.id.mnComprasFeitas){
                val i = Intent(this@TelaMenuInicialActivity, ListagemVendasActivity::class.java)
                startActivity(i)
            }

            if(it.itemId === R.id.mnFazerLogout){
                finish()
                FirebaseAuth.getInstance().signOut()
            }

            if(it.itemId === R.id.mnSairAplicativo){
                dialogoFecharAplicaçao()
            }

            false
        }
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (toggle!!.onOptionsItemSelected(item)) {
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        if(getCurrentUser() != null){
            obterProdutos()
        }
    }

    override fun onBackPressed() {
        dialogoFecharAplicaçao()
    }

    fun dialogoFecharAplicaçao(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Fechar Aplicação")
        builder.setMessage("Quer mesmo fechar o aplicativo?")
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            finishAffinity()
        }
        builder.setNegativeButton(android.R.string.no) { dialog, which ->
        }
        builder.show()
    }


    fun obterProdutosPorNome(nomeProduto: String){
        val progressDialog = ProgressDialog(this@TelaMenuInicialActivity)
        progressDialog.setMessage("Buscando por nome.")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val retrofit4 = Retrofit.Builder()
            .baseUrl("https://oficinacordova.azurewebsites.net")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service4 = retrofit4.create(ProdutoService::class.java)
        val call4 = service4.obterPorNome(nomeProduto)
        val callback4 = object : Callback<List<Produto>> {

            override fun onResponse(call: Call<List<Produto>>, response: Response<List<Produto>>) {
                if(response.isSuccessful){
                    atualizarUi(response.body())
                    Handler().postDelayed({progressDialog.dismiss()}, 1000)
                } else{
                    Snackbar
                        .make(containerMenu, "Não foi possível atualizar os produtos", Snackbar.LENGTH_LONG)
                        .show()
                    Log.e("ERRO", response.errorBody().toString())
                    Handler().postDelayed({progressDialog.dismiss()}, 1000)
                }
            }
            override fun onFailure(call: Call<List<Produto>>, t: Throwable) {
                Snackbar
                        .make(containerMenu, "Não foi possível se conectar a internet", Snackbar.LENGTH_LONG)
                    .show()
                Log.e("ERRO", "Falha ao chamar o serviço", t)
                Handler().postDelayed({progressDialog.dismiss()}, 1000)
            }

        }
        call4.enqueue(callback4)
    }

    fun obterProdutosPorCategoria(idCategoria: Int){
        val progressDialog = ProgressDialog(this@TelaMenuInicialActivity)
        progressDialog.setMessage("Buscando por categoria.")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val retrofit3 = Retrofit.Builder()
            .baseUrl("https://oficinacordova.azurewebsites.net")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service3 = retrofit3.create(ProdutoService::class.java)
        val call3 = service3.obterPorCategoria(idCategoria)
        val callback3 = object : Callback<List<Produto>> {

            override fun onResponse(call: Call<List<Produto>>, response: Response<List<Produto>>) {
                if(response.isSuccessful){
                    atualizarUi(response.body())
                    Handler().postDelayed({progressDialog.dismiss()}, 1000)
                } else{
                    Snackbar
                        .make(containerMenu, "Não foi possível atualizar os produtos", Snackbar.LENGTH_LONG)
                        .show()
                    Log.e("ERRO", response.errorBody().toString())
                    Handler().postDelayed({progressDialog.dismiss()}, 1000)
                }
            }
            override fun onFailure(call: Call<List<Produto>>, t: Throwable) {
                Snackbar
                    .make(containerMenu, "Não foi possível se conectar a internet", Snackbar.LENGTH_LONG)
                    .show()
                Log.e("ERRO", "Falha ao chamar o serviço", t)
                Handler().postDelayed({progressDialog.dismiss()}, 1000)
            }

        }
        call3.enqueue(callback3)
    }

    fun obterProdutos(){
        val progressDialog = ProgressDialog(this@TelaMenuInicialActivity)
        progressDialog.setMessage("Carregando produtos.")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val retrofit = Retrofit.Builder()
            .baseUrl("https://oficinacordova.azurewebsites.net")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(ProdutoService::class.java)
        val call = service.list()
        val callback = object : Callback<List<Produto>> {

            override fun onResponse(call: Call<List<Produto>>, response: Response<List<Produto>>) {
                if(response.isSuccessful){
                    atualizarUi(response.body())
                    Handler().postDelayed({progressDialog.dismiss()}, 1000)
                } else{
                    Snackbar
                        .make(containerMenu, "Não foi possível atualizar os produtos", Snackbar.LENGTH_LONG)
                        .show()
                    Log.e("ERRO", response.errorBody().toString())
                    Handler().postDelayed({progressDialog.dismiss()}, 1000)
                }
            }
            override fun onFailure(call: Call<List<Produto>>, t: Throwable) {
                Snackbar
                    .make(containerMenu, "Não foi possível se conectar a internet", Snackbar.LENGTH_LONG)
                    .show()
                Log.e("ERRO", "Falha ao chamar o serviço", t)
                Handler().postDelayed({progressDialog.dismiss()}, 1000)
            }

        }
        call.enqueue(callback)
    }

    fun atualizarUi(listaProdutos: List<Produto>?){
        containerMenu.removeAllViews()
        val formater = NumberFormat.getCurrencyInstance()
        if(listaProdutos != null){
            for(produto in listaProdutos){
                val cardView = layoutInflater
                    .inflate(R.layout.card_item, containerMenu, false)
                cardView.txtPreco.text = formater.format(produto.precProduto)
                cardView.txtNome.text = produto.nomeProduto
                cardView.btnDetalhe.setOnClickListener{
                    val inte = Intent(this@TelaMenuInicialActivity, DetalheProdutoActivity::class.java)
                    inte.putExtra("idProduto", produto.idProduto)
                    startActivity(inte)
                }
                cardView.btnCarrinho.setOnClickListener{
                    val intentCarrinho = Intent(this@TelaMenuInicialActivity, AdicionaCarrinhoActivity::class.java)
                    intentCarrinho.putExtra("idProduto", produto.idProduto)
                    startActivityForResult(intentCarrinho, 1)
                }
                Picasso.get().load("https://oficinacordova.azurewebsites.net/android/rest/produto/image/"
                        + produto.idProduto)
                    .into(cardView.imgProduto)
                containerMenu.addView(cardView)
            }
        }
    }

    fun obterCategoriasProduto(): ArrayList<String> {
        var listaRetorno = arrayListOf<String>("Selecione a categoria")
        var categoriaVazia = Categoria("Filtro vazio", "Selecione...", 0)
        val retrofit2 = Retrofit.Builder()
            .baseUrl("https://oficinacordova.azurewebsites.net")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service2 = retrofit2.create(CategoriaService::class.java)
        val call2 = service2.list()

        val callback2 = object : Callback<List<Categoria>> {
            override fun onResponse(call: Call<List<Categoria>>, response: Response<List<Categoria>>) {
                if(response.isSuccessful){
                    var lista = response.body()
                    if(lista!= null){
                        for(item in lista){
                            listaRetorno.add(item.nomeCategoria)
                        }
                    }
                } else{
                    Snackbar
                        .make(containerMenu, "Não foi possível atualizar os produtos", Snackbar.LENGTH_LONG)
                        .show()
                    Log.e("ERRO", response.errorBody().toString())
                }
            }
            override fun onFailure(call: Call<List<Categoria>>, t: Throwable) {
                Snackbar
                    .make(containerMenu, "Não foi possível se conectar a internet", Snackbar.LENGTH_LONG)
                    .show()
                Log.e("ERRO", "Falha ao chamar o serviço", t)
            }

        }
        call2.enqueue(callback2)
        return listaRetorno
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                Toast.makeText(this, "O produto " + data?.getStringExtra("nomeProduto") + " foi adicionado com sucesso ao carrinho.", Toast.LENGTH_LONG).show()
            }
        }

        if(requestCode == 2){
            if(resultCode == Activity.RESULT_OK){
                Toast.makeText(this, "A compra no valor de " + data?.getStringExtra("valorCompra") + " foi realizada com sucesso!", Toast.LENGTH_LONG).show()
            }
        }

    }

    fun getCurrentUser(): FirebaseUser?{
        val auth = FirebaseAuth.getInstance()
        return auth.currentUser
    }
}
