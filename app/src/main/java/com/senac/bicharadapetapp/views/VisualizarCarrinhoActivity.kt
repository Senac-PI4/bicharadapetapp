package com.senac.bicharadapetapp.views

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.senac.bicharadapetapp.R
import com.senac.bicharadapetapp.model.Carrinho
import com.senac.bicharadapetapp.model.ItemCarrinho
import com.senac.bicharadapetapp.model.Venda
import kotlinx.android.synthetic.main.activity_visualizar_carrinho.*
import kotlinx.android.synthetic.main.activity_visualizar_carrinho.view.*
import kotlinx.android.synthetic.main.card_item_carrinho.view.*
import java.text.NumberFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class VisualizarCarrinhoActivity : AppCompatActivity() {

    var database: DatabaseReference? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visualizar_carrinho)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED;
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        configurarFirebase()
        containerCarrinho.removeAllViews()
        val formater = NumberFormat.getCurrencyInstance()
        if(Carrinho.carrinholista != null){
            montaTelaVerCarrinho(Carrinho.carrinholista)
        }

        if(Carrinho.carrinholista.isEmpty()){
            btnEfetuarCompra.visibility = android.view.View.INVISIBLE
            txtLabelTotalDoCarrinhoActivity.visibility = android.view.View.INVISIBLE
            txtValorTotalCarrinhoAct.visibility = android.view.View.INVISIBLE
                val cardView = layoutInflater
                    .inflate(R.layout.card_item_carrinho_vazio, containerCarrinho, false)
            containerCarrinho.addView(cardView)
            Snackbar
                .make(containerCarrinho, "O carrinho está vazio!", Snackbar.LENGTH_LONG)
                .show()
        } else {
            txtValorTotalCarrinhoAct.text = formater.format(calculaTotalDoCarrinho())
            btnEfetuarCompra.setOnClickListener{
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Finalizar compra")
                builder.setMessage("Quer mesmo finalizar a compra?")
                builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                    realizarCompra()
                }
                builder.setNegativeButton(android.R.string.no) { dialog, which ->

                }
                builder.show()
            }
        }

    }

    fun montaTelaVerCarrinho(listaProdutosCarrinho: List<ItemCarrinho>){

        if(listaProdutosCarrinho != null){
            containerCarrinho.removeAllViews()
            val formater = NumberFormat.getCurrencyInstance()
            for(itemCarrinho in Carrinho.carrinholista){
                val cardView = layoutInflater
                    .inflate(R.layout.card_item_carrinho, containerCarrinho, false)
                cardView.txtNomeCarrinho.text = itemCarrinho?.produto.nomeProduto
                cardView.txtPrecoCarrinho.text = formater.format(itemCarrinho?.produto.precProduto)
                cardView.txtQuantidadeProdutoCarrinho.text = itemCarrinho.quantidade.toString()
                cardView.txtDescontoDoProdutoCardCarrinho.text = formater.format(itemCarrinho?.produto.descontoPromocao)
                cardView.btnRemoverItemDoCarrinho.setOnClickListener{
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Remover item")
                    builder.setMessage("Quer mesmo remover este item do carrinho?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                        removerItemDoCarrinho(itemCarrinho)
                    }
                    builder.setNegativeButton(android.R.string.no) { dialog, which ->
                    }
                    builder.show()
                }
                containerCarrinho.addView(cardView)
            }
        }
    }

    fun removerItemDoCarrinho(item: ItemCarrinho){
        Carrinho.carrinholista.remove(item)
        if(Carrinho.carrinholista.size == 0){
            containerCarrinho.removeAllViews()
            btnEfetuarCompra.visibility = android.view.View.INVISIBLE
            txtLabelTotalDoCarrinhoActivity.visibility = android.view.View.INVISIBLE
            txtValorTotalCarrinhoAct.visibility = android.view.View.INVISIBLE
            val cardView = layoutInflater
                .inflate(R.layout.card_item_carrinho_vazio, containerCarrinho, false)
            containerCarrinho.addView(cardView)
            Snackbar
                .make(containerCarrinho, "O carrinho está vazio!", Snackbar.LENGTH_LONG)
                .show()
        }
        if(Carrinho.carrinholista.isNotEmpty()){
            montaTelaVerCarrinho(Carrinho.carrinholista)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun realizarCompra() {
        val formater = NumberFormat.getCurrencyInstance()
        val novaVenda = database?.child("venda")?.push()
        val idVenda = novaVenda?.key
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        var dataAtual =  current.format(formatter)
        var qtdTotalItensCarrinho = 0
        var totalVenda = 0.0
        for(item in Carrinho.carrinholista){
            totalVenda += ((item.produto.precProduto-item.produto.descontoPromocao)*item.quantidade)
            qtdTotalItensCarrinho+= item.quantidade
        }
        var venda = Venda(produtos = Carrinho.carrinholista, dataVenda = dataAtual, id = idVenda, totalVenda = totalVenda, qtdTotalItensCarrinho = qtdTotalItensCarrinho)
        novaVenda?.setValue(venda)
        Carrinho.carrinholista.clear()
        var intentRetorno = Intent()

        intentRetorno.putExtra("valorCompra", formater.format(totalVenda))
        setResult(Activity.RESULT_OK, intentRetorno)
        finish()
    }

    fun calculaTotalDoCarrinho(): Double{
        var totalCarrinho = 0.0
        for(item in Carrinho.carrinholista){
            totalCarrinho += ((item.produto.precProduto-item.produto.descontoPromocao)*item.quantidade)
        }
        return totalCarrinho
    }

    fun configurarFirebase() {
        val usuario = getCurrentUser()
        if(usuario != null){
            database = FirebaseDatabase.getInstance().reference.child(usuario.uid)
        }
    }

    fun getCurrentUser(): FirebaseUser?{
        val auth = FirebaseAuth.getInstance()
        return auth.currentUser
    }
}
