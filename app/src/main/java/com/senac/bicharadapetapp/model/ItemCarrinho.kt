package com.senac.bicharadapetapp.model

data class ItemCarrinho (
    var quantidade: Int,
    var produto: Produto
)