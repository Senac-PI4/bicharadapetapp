package com.senac.bicharadapetapp.model

import java.util.*

data class Venda (
    var id: String? = null,
    var dataVenda: String,
    var totalVenda: Double,
    var produtos: List<ItemCarrinho>,
    var qtdTotalItensCarrinho: Int
)