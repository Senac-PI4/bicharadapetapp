package com.senac.bicharadapetapp.model

data class Categoria (
    val descCategoria: String,
    val nomeCategoria: String,
    val idCategoria: Int
)
