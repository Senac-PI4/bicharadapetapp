package com.senac.bicharadapetapp.services

import com.senac.bicharadapetapp.model.Categoria
import retrofit2.Call
import retrofit2.http.GET

interface CategoriaService {
    @GET("/android/rest/categoria")
    fun list(): Call<List<Categoria>>;
}