package com.senac.bicharadapetapp.services

import com.senac.bicharadapetapp.model.Produto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ProdutoService {
    @GET("/android/rest/produto")
    fun list(): Call<List<Produto>>;

    @GET("/android/rest/produto/{idProduto}")
    fun obterPorId(@Path("idProduto") idProduto: Int): Call<Produto>;

    @GET("/android/rest/produto/categoria/{idCategoria}")
    fun obterPorCategoria(@Path("idCategoria") idCategoria: Int): Call<List<Produto>>;

    @GET("/android/rest/produto/{nomeProduto}")
    fun obterPorNome(@Path("nomeProduto") nomeProduto: String): Call<List<Produto>>;
}